<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib uri = "http://java.sun.com/jsp/jstl/core" prefix = "c" %>
<html>
<head>
    <link href="/styles/styles.css" rel="stylesheet" type="text/css">
    <title>User phones</title>
</head>
<body>
    <div class="tableCont">
        <table>
            <tr>
                <th>Phones of the selected user</th>
            </tr>
            <c:forEach items="${userPhones}" var="phone">
                <tr>
                    <td>${phone.model}</td>
                </tr>
            </c:forEach>
        </table>
    </div>
</body>
</html>
