<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib uri = "http://java.sun.com/jsp/jstl/core" prefix = "c" %>
<html>
<head>
    <link href="/styles/styles.css" rel="stylesheet" type="text/css">
    <meta charset="utf-8">
    <title>Users</title>
</head>
<body>
        <div class="search">
            <form method="post" action="/">
                <input class="searchInput" type="text" name="first_name" placeholder="First Name" required>
                <input class="searchButton" type="submit" value="Search">
            </form>
        </div>
        <div class="tableCont">
            <table>
                <tr>
                    <th>First Name</th>
                    <th>Last Name</th>
                    <th>Phone</th>
                </tr>
                <c:forEach items="${usersFromDB}" var="user">
                    <tr>
                        <td>${user.firstName}</td>
                        <td>${user.lastName}</td>
                        <td><a class="ref" href="/${user.id}">View</a></td>
                    </tr>
                </c:forEach>
            </table>
        </div>
</body>
</html>
