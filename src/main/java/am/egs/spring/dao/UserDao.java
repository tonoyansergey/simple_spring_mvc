package am.egs.spring.dao;

import am.egs.spring.models.Phone;
import am.egs.spring.models.User;

import java.util.List;

public interface UserDao extends CrudDao<User> {

    List<User> findByFirstName(String firstName);

    List<Phone> findPhones(Long id);
}
