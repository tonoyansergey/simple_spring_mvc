package am.egs.spring.dao;

import am.egs.spring.controllers.UsersController;
import am.egs.spring.models.Phone;
import am.egs.spring.models.User;
import lombok.NoArgsConstructor;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Component;
import javax.sql.DataSource;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

@NoArgsConstructor
@Component
public class UserDaoImpl implements UserDao {

    private static final Logger logger = Logger.getLogger(UsersController.class);
    JdbcTemplate template;

    //language=SQL
    private final String SQL_SELECT_ALL_USERS =
            "SELECT * FROM users";

    private final String SQL_SELECT_ALL_WITH_PHONES =
            "SELECT users.id, users.first_name, users.last_name, phones.model, phones.id as phone_id from users left join phones on users.id = phones.owner_id";

    private final String SQL_SELECT_USER_BY_FIRST_NAME =
            "SELECT * FROM users WHERE first_name LIKE ?";

    private final String SQL_SELECT_PHONES_BY_USER_ID =
            "SELECT * FROM phones WHERE owner_id = ?";

    private final String SQL_SELECT_USER_BY_ID =
            "SELECT * FROM users WHERE id = :id";

    private final String SQL_INSERT_USER =
            "INSERT INTO users (first_name, last_name) VALUES (:firstName, :lastName)";

    RowMapper<User> userWithPhoneRowMapper = new RowMapper<User>() {
        @Override
        public User mapRow(ResultSet rs, int rowNum) throws SQLException {

            Long id = rs.getLong("id");
            String firstName = rs.getString("first_name");
            String lastName = rs.getString("last_name");

            User user = new User(id, firstName, lastName, new ArrayList<>());

            Long phoneId = rs.getLong("phone_id");
            String phoneModel = rs.getString("model");

            user.getPhoneList().add(new Phone(phoneId, phoneModel, user));

            return user;
        }
    };

    RowMapper<User> userRowMapper = new RowMapper<User>() {
        @Override
        public User mapRow(ResultSet rs, int rowNum) throws SQLException {
            return User.builder()
                    .id(rs.getLong("id"))
                    .firstName(rs.getString("first_name"))
                    .lastName(rs.getString("last_name"))
                    .build();

        }
    };

    RowMapper<Phone> phoneRowMapper = new RowMapper<Phone>() {
        @Override
        public Phone mapRow(ResultSet rs, int rowNum) throws SQLException {
            return Phone.builder()
                    .id(rs.getLong("id"))
                    .model(rs.getString("model"))
                    .build();
        }
    };

    @Autowired
    public UserDaoImpl(DataSource dataSource) {
        this.template = new JdbcTemplate(dataSource);
    }
    @Override

    public List<User> findAll() {
        return template.query(SQL_SELECT_ALL_USERS, userRowMapper);
    }

    @Override
    public List<User> findByFirstName(String firstName) {
        firstName = "%" + firstName + "%";
        return template.query(SQL_SELECT_USER_BY_FIRST_NAME, userRowMapper, firstName);
    }

    @Override
    public List<Phone> findPhones(Long id) {
        return template.query(SQL_SELECT_PHONES_BY_USER_ID, phoneRowMapper, id);
    }

    @Override
    public User find(Long id) {
        return null;
    }

    @Override
    public void save(User model) {

    }

    @Override
    public void update(User model) {

    }

    @Override
    public void delete(Long id) {

    }
}

