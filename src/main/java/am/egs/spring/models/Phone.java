package am.egs.spring.models;

import lombok.*;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
@ToString(exclude = "owner")
public class Phone {

    private Long id;
    private String model;
    private User owner;
}
