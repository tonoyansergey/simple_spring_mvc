package am.egs.spring.controllers;

import am.egs.spring.dao.UserDao;
import am.egs.spring.models.Phone;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

import java.util.List;

@Controller
public class PhonesController {

    private static final Logger logger = Logger.getLogger(UsersController.class);

    @Autowired
    private UserDao userDao;

    @RequestMapping(path = "/{user-id}", method = RequestMethod.GET)
    public ModelAndView getUserPhones (@PathVariable("user-id") Long userId) {

        List<Phone> phoneList = userDao.findPhones(userId);
        logger.debug("Getting phones of the selected user from db");
        ModelAndView modelAndView = new ModelAndView("phones");
        modelAndView.addObject("userPhones", phoneList);
        return modelAndView;
    }
}
