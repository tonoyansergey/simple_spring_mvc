package am.egs.spring.controllers;

import am.egs.spring.dao.UserDao;
import am.egs.spring.models.User;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;
import java.util.List;

@Controller
public class UsersController {

    private static final Logger logger = Logger.getLogger(UsersController.class);


    @Autowired
    private UserDao userDao;

    @RequestMapping(path = "/", method = RequestMethod.GET)
    public ModelAndView getAllUsers() {

        List<User> userList = userDao.findAll();
        logger.debug("Getting all users from db");
        ModelAndView modelAndView = new ModelAndView("users");
        modelAndView.addObject("usersFromDB", userList);
        return modelAndView;

    }

    @RequestMapping(path = "/", method = RequestMethod.POST)
    public ModelAndView getByFirstName(@RequestParam("first_name") String firstName) {

        List<User> userList = null;
        if (firstName != null) {
            userList = userDao.findByFirstName(firstName);
            logger.debug("Searching user by keyword");
        }
        ModelAndView modelAndView = new ModelAndView("users");
        modelAndView.addObject("usersFromDB", userList);
        return modelAndView;
    }
}
